
function diskon(x) {
    let musimPandemik = (x * 30)/100
    return musimPandemik
    //return (x * 30) / 100 //(bisa pakai ini biar cepet); return tidak bisa ada 2 dalam 1 function; fungsi return untuk debugging dan juga penentuan.
}

let sale = diskon(20000)
console.log(sale) //output: 6000

//interjer
function sayHiTo(name) {
    let halo = `Hai ${name.toUpperCase()}`
    return halo
}

let test1 = sayHiTo 