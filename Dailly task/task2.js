// 1. bikin class Vehicle (Kendaraan)
// attribut nya (constructor) : jenis kendaraan (roda berapa 2/4 atau lainnya), negara produksi, 
// ada method info yang melakukan print = 'Jenis Kendaraan roda 4 dari negara Jerman';
class Vehicle {
    // make a class vehicle with attribut modifiedVehicle dan productionCountry;
    constructor(modifiedVehicle, productionCountry) {
        // add constructor
        this.modifiedVehicle = modifiedVehicle;
        this.productionCountry = productionCountry;
        // make attribut
    }
    info() {
        // add instance methode for print the class vehicle attribut
        console.log(`Jenis Kendaraan roda ${this.modifiedVehicle} dari Negara ${this.productionCountry}`);
    }
}
// 2. bikin child class (Mobil) dari Kendaraan, inherit attribut jenis kendaraan dan negara produksi dari super/parent class nya
// attribut baru di child class ini yaitu Merek Kendaraan, Harga Kendaraan dan Persen Pajak
class Cars extends Vehicle {
    // make child class Cars with inherritance from class Vehicle
    constructor(modifiedVehicle, productionCountry, productionVehicle, price, beaCukai) {
        // add constructor for this class
        super(modifiedVehicle, productionCountry)
            // call super class/parent class constructor
        this.productionVehicle = productionVehicle;
        this.price = price;
        this.beaCukai = beaCukai;
        // make a new attribute inside the child class
    }
    totalPrice() {
        // make a total price method to return count of price sum with Bea Cukai
        return this.price + (this.price * this.beaCukai / 100);
    }
    info() {
        // make override info method
        super.info();
        // call parent info method 
        console.log(`Kendaraan ini merknya ${this.merekKendaraan} dengan harga Rp. ${this.totalPrice()}`);
        // make a print from merk and price
    }
}
// 3. ada method totalPrice yang melakukan proses menambah harga normal dengan persen pajak, yang RETURN hasil penjumlahan tersebut
// 4. overidding method info dari super/parent class (panggil instance method info dari super class = super.methodName() dan di overriding method info di child class ini ada tambahan =>
// print = 'Kendaraan ini nama mereknya Mercedes dengan total harga Rp. 880.000.000'

// 5. buat 2 instance (1 dari parent class(Vehicle), 1 dari child class aja(Mobil)) 

// make 2 instance variable 1 from parent class and 1 from child class
const kendaraan = new Vehicle(2, 'Korean')
kendaraan.info()
const cars = new Cars (4, 'Japan', 'Germany', 1405000000, 10);
mobil.info()
// contoh instance
// 1. const kendaraan = new Vehicle(2, 'Jepang')
// kendaraan.info()
// 2. const mobil = new Mobil(4, 'Jerman', 'Mercedes', 800000000, 10);
// mobil.info()

/** 
    NOTES
    rumus total price setelah di tambah pajak utk method totalPrice, bisa kalian googling sendiri yah.
*/
